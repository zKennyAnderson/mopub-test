﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MoPubBase/AdvancedBidder[]
struct AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08;
// MoPubBase/MediationSetting[]
struct MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B;
// MoPubBase/RewardedNetwork[]
struct RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.String>
struct Action_1_t19CAF500829927B30EC94F39939F749E4919669E;
// System.Action`2<System.String,System.Single>
struct Action_2_tD053D0BAA29F99F68652212454226A67BE949793;
// System.Action`2<System.String,System.String>
struct Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3;
// System.Action`3<MoPubBase/Consent/Status,MoPubBase/Consent/Status,System.Boolean>
struct Action_3_t94FD4401FD739E608B7CC5F8918FAF026C04DA76;
// System.Action`3<System.String,System.String,System.Single>
struct Action_3_tE127DFBA1D4D2336BD8421F6F97C63D472CEFE6C;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD;
// System.Collections.Generic.Dictionary`2<MoPubBase/Reward,UnityEngine.AndroidJavaObject>
struct Dictionary_2_tE37A2BCB9323DEBF5F84FB8F857D2891251A9CD5;
// System.Collections.Generic.Dictionary`2<System.String,MoPubAndroidBanner>
struct Dictionary_2_tAA807BD4DFDBC31BB8F9CF538DE207457F6EEFF6;
// System.Collections.Generic.Dictionary`2<System.String,MoPubAndroidInterstitial>
struct Dictionary_2_tF1D3EF35F4BEFABA4F50501ADAFDB926C03C07B9;
// System.Collections.Generic.Dictionary`2<System.String,MoPubAndroidRewardedVideo>
struct Dictionary_2_t13EFE8B9F65DAA94704EC73D62FD719DFE42A3D6;
// System.Collections.Generic.Dictionary`2<System.String,MoPubBinding>
struct Dictionary_2_t318F315F43DFED97F57079D2C81F0C6844934105;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Func`2<MoPubBase/AdvancedBidder,System.String>
struct Func_2_tA7AF46BEB915C0AF9563A355DE09B3F84A7D658E;
// System.Func`2<MoPubBase/RewardedNetwork,System.String>
struct Func_2_tE02BF822F0217133F63D80338895546BB7789FEB;
// System.Func`2<System.Object,System.String>
struct Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF;
// System.Func`2<System.String,System.String[]>
struct Func_2_tC986729FEB8E089946D488363CA1463CFF8E3F3A;
// System.Func`2<System.String[],System.Boolean>
struct Func_2_tFBCB4F49C9CE9872B17F374093FDCAB449E61F15;
// System.IO.StringReader
struct StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PARTNERAPI_T7E4194C1D7C65B28ECC9ABFCD9562E952A709C35_H
#define PARTNERAPI_T7E4194C1D7C65B28ECC9ABFCD9562E952A709C35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubAndroid/PartnerApi
struct  PartnerApi_t7E4194C1D7C65B28ECC9ABFCD9562E952A709C35  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTNERAPI_T7E4194C1D7C65B28ECC9ABFCD9562E952A709C35_H
#ifndef MOPUBANDROIDBANNER_T654343028A79D05456C69A16B1A1CA00880413C1_H
#define MOPUBANDROIDBANNER_T654343028A79D05456C69A16B1A1CA00880413C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubAndroidBanner
struct  MoPubAndroidBanner_t654343028A79D05456C69A16B1A1CA00880413C1  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject MoPubAndroidBanner::_bannerPlugin
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ____bannerPlugin_0;

public:
	inline static int32_t get_offset_of__bannerPlugin_0() { return static_cast<int32_t>(offsetof(MoPubAndroidBanner_t654343028A79D05456C69A16B1A1CA00880413C1, ____bannerPlugin_0)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get__bannerPlugin_0() const { return ____bannerPlugin_0; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of__bannerPlugin_0() { return &____bannerPlugin_0; }
	inline void set__bannerPlugin_0(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		____bannerPlugin_0 = value;
		Il2CppCodeGenWriteBarrier((&____bannerPlugin_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBANDROIDBANNER_T654343028A79D05456C69A16B1A1CA00880413C1_H
#ifndef MOPUBANDROIDINTERSTITIAL_T087506AE648C1295D36DCA16338F0DA08F4E5702_H
#define MOPUBANDROIDINTERSTITIAL_T087506AE648C1295D36DCA16338F0DA08F4E5702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubAndroidInterstitial
struct  MoPubAndroidInterstitial_t087506AE648C1295D36DCA16338F0DA08F4E5702  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject MoPubAndroidInterstitial::_interstitialPlugin
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ____interstitialPlugin_0;

public:
	inline static int32_t get_offset_of__interstitialPlugin_0() { return static_cast<int32_t>(offsetof(MoPubAndroidInterstitial_t087506AE648C1295D36DCA16338F0DA08F4E5702, ____interstitialPlugin_0)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get__interstitialPlugin_0() const { return ____interstitialPlugin_0; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of__interstitialPlugin_0() { return &____interstitialPlugin_0; }
	inline void set__interstitialPlugin_0(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		____interstitialPlugin_0 = value;
		Il2CppCodeGenWriteBarrier((&____interstitialPlugin_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBANDROIDINTERSTITIAL_T087506AE648C1295D36DCA16338F0DA08F4E5702_H
#ifndef MOPUBANDROIDREWARDEDVIDEO_T6CAD3CA835B573B2F6BB715FACB436EE440DB08E_H
#define MOPUBANDROIDREWARDEDVIDEO_T6CAD3CA835B573B2F6BB715FACB436EE440DB08E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubAndroidRewardedVideo
struct  MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject MoPubAndroidRewardedVideo::_plugin
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ____plugin_1;
	// System.Collections.Generic.Dictionary`2<MoPubBase/Reward,UnityEngine.AndroidJavaObject> MoPubAndroidRewardedVideo::_rewardsDict
	Dictionary_2_tE37A2BCB9323DEBF5F84FB8F857D2891251A9CD5 * ____rewardsDict_2;

public:
	inline static int32_t get_offset_of__plugin_1() { return static_cast<int32_t>(offsetof(MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E, ____plugin_1)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get__plugin_1() const { return ____plugin_1; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of__plugin_1() { return &____plugin_1; }
	inline void set__plugin_1(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		____plugin_1 = value;
		Il2CppCodeGenWriteBarrier((&____plugin_1), value);
	}

	inline static int32_t get_offset_of__rewardsDict_2() { return static_cast<int32_t>(offsetof(MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E, ____rewardsDict_2)); }
	inline Dictionary_2_tE37A2BCB9323DEBF5F84FB8F857D2891251A9CD5 * get__rewardsDict_2() const { return ____rewardsDict_2; }
	inline Dictionary_2_tE37A2BCB9323DEBF5F84FB8F857D2891251A9CD5 ** get_address_of__rewardsDict_2() { return &____rewardsDict_2; }
	inline void set__rewardsDict_2(Dictionary_2_tE37A2BCB9323DEBF5F84FB8F857D2891251A9CD5 * value)
	{
		____rewardsDict_2 = value;
		Il2CppCodeGenWriteBarrier((&____rewardsDict_2), value);
	}
};

struct MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass MoPubAndroidRewardedVideo::PluginClass
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___PluginClass_0;

public:
	inline static int32_t get_offset_of_PluginClass_0() { return static_cast<int32_t>(offsetof(MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E_StaticFields, ___PluginClass_0)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_PluginClass_0() const { return ___PluginClass_0; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_PluginClass_0() { return &___PluginClass_0; }
	inline void set_PluginClass_0(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___PluginClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___PluginClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBANDROIDREWARDEDVIDEO_T6CAD3CA835B573B2F6BB715FACB436EE440DB08E_H
#ifndef MOPUBBASE_T4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_H
#define MOPUBBASE_T4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase
struct  MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434  : public RuntimeObject
{
public:

public:
};

struct MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields
{
public:
	// System.String MoPubBase::<ConsentLanguageCode>k__BackingField
	String_t* ___U3CConsentLanguageCodeU3Ek__BackingField_0;
	// System.String MoPubBase::moPubSDKVersion
	String_t* ___moPubSDKVersion_2;
	// System.String MoPubBase::_pluginName
	String_t* ____pluginName_3;

public:
	inline static int32_t get_offset_of_U3CConsentLanguageCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields, ___U3CConsentLanguageCodeU3Ek__BackingField_0)); }
	inline String_t* get_U3CConsentLanguageCodeU3Ek__BackingField_0() const { return ___U3CConsentLanguageCodeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CConsentLanguageCodeU3Ek__BackingField_0() { return &___U3CConsentLanguageCodeU3Ek__BackingField_0; }
	inline void set_U3CConsentLanguageCodeU3Ek__BackingField_0(String_t* value)
	{
		___U3CConsentLanguageCodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConsentLanguageCodeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_moPubSDKVersion_2() { return static_cast<int32_t>(offsetof(MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields, ___moPubSDKVersion_2)); }
	inline String_t* get_moPubSDKVersion_2() const { return ___moPubSDKVersion_2; }
	inline String_t** get_address_of_moPubSDKVersion_2() { return &___moPubSDKVersion_2; }
	inline void set_moPubSDKVersion_2(String_t* value)
	{
		___moPubSDKVersion_2 = value;
		Il2CppCodeGenWriteBarrier((&___moPubSDKVersion_2), value);
	}

	inline static int32_t get_offset_of__pluginName_3() { return static_cast<int32_t>(offsetof(MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields, ____pluginName_3)); }
	inline String_t* get__pluginName_3() const { return ____pluginName_3; }
	inline String_t** get_address_of__pluginName_3() { return &____pluginName_3; }
	inline void set__pluginName_3(String_t* value)
	{
		____pluginName_3 = value;
		Il2CppCodeGenWriteBarrier((&____pluginName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBBASE_T4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_H
#ifndef CONSENT_T48C9151900FAD71E084A92493B822A1514CA681E_H
#define CONSENT_T48C9151900FAD71E084A92493B822A1514CA681E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/Consent
struct  Consent_t48C9151900FAD71E084A92493B822A1514CA681E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSENT_T48C9151900FAD71E084A92493B822A1514CA681E_H
#ifndef STRINGS_T2C56F329FE6C514D38DB9364EE4F5DA76BC895E4_H
#define STRINGS_T2C56F329FE6C514D38DB9364EE4F5DA76BC895E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/Consent/Strings
struct  Strings_t2C56F329FE6C514D38DB9364EE4F5DA76BC895E4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGS_T2C56F329FE6C514D38DB9364EE4F5DA76BC895E4_H
#ifndef U3CU3EC_TEE373A695EB3F985F0545801FD3DE5468A905E23_H
#define U3CU3EC_TEE373A695EB3F985F0545801FD3DE5468A905E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/SdkConfiguration/<>c
struct  U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields
{
public:
	// MoPubBase/SdkConfiguration/<>c MoPubBase/SdkConfiguration/<>c::<>9
	U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23 * ___U3CU3E9_0;
	// System.Func`2<MoPubBase/AdvancedBidder,System.String> MoPubBase/SdkConfiguration/<>c::<>9__5_0
	Func_2_tA7AF46BEB915C0AF9563A355DE09B3F84A7D658E * ___U3CU3E9__5_0_1;
	// System.Func`2<MoPubBase/RewardedNetwork,System.String> MoPubBase/SdkConfiguration/<>c::<>9__9_0
	Func_2_tE02BF822F0217133F63D80338895546BB7789FEB * ___U3CU3E9__9_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_tA7AF46BEB915C0AF9563A355DE09B3F84A7D658E * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_tA7AF46BEB915C0AF9563A355DE09B3F84A7D658E ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_tA7AF46BEB915C0AF9563A355DE09B3F84A7D658E * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields, ___U3CU3E9__9_0_2)); }
	inline Func_2_tE02BF822F0217133F63D80338895546BB7789FEB * get_U3CU3E9__9_0_2() const { return ___U3CU3E9__9_0_2; }
	inline Func_2_tE02BF822F0217133F63D80338895546BB7789FEB ** get_address_of_U3CU3E9__9_0_2() { return &___U3CU3E9__9_0_2; }
	inline void set_U3CU3E9__9_0_2(Func_2_tE02BF822F0217133F63D80338895546BB7789FEB * value)
	{
		___U3CU3E9__9_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TEE373A695EB3F985F0545801FD3DE5468A905E23_H
#ifndef THIRDPARTYNETWORK_T0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6_H
#define THIRDPARTYNETWORK_T0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/ThirdPartyNetwork
struct  ThirdPartyNetwork_t0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6  : public RuntimeObject
{
public:
	// System.String MoPubBase/ThirdPartyNetwork::_name
	String_t* ____name_0;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(ThirdPartyNetwork_t0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THIRDPARTYNETWORK_T0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6_H
#ifndef U3CU3EC_T097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_H
#define U3CU3EC_T097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBinding/<>c
struct  U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields
{
public:
	// MoPubBinding/<>c MoPubBinding/<>c::<>9
	U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String[]> MoPubBinding/<>c::<>9__16_0
	Func_2_tC986729FEB8E089946D488363CA1463CFF8E3F3A * ___U3CU3E9__16_0_1;
	// System.Func`2<System.String[],System.Boolean> MoPubBinding/<>c::<>9__16_1
	Func_2_tFBCB4F49C9CE9872B17F374093FDCAB449E61F15 * ___U3CU3E9__16_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Func_2_tC986729FEB8E089946D488363CA1463CFF8E3F3A * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Func_2_tC986729FEB8E089946D488363CA1463CFF8E3F3A ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Func_2_tC986729FEB8E089946D488363CA1463CFF8E3F3A * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields, ___U3CU3E9__16_1_2)); }
	inline Func_2_tFBCB4F49C9CE9872B17F374093FDCAB449E61F15 * get_U3CU3E9__16_1_2() const { return ___U3CU3E9__16_1_2; }
	inline Func_2_tFBCB4F49C9CE9872B17F374093FDCAB449E61F15 ** get_address_of_U3CU3E9__16_1_2() { return &___U3CU3E9__16_1_2; }
	inline void set_U3CU3E9__16_1_2(Func_2_tFBCB4F49C9CE9872B17F374093FDCAB449E61F15 * value)
	{
		___U3CU3E9__16_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T2FAEADCC188F29AD24FCE5D699BD39076869B841_H
#define U3CU3EC__DISPLAYCLASS16_0_T2FAEADCC188F29AD24FCE5D699BD39076869B841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBinding/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t2FAEADCC188F29AD24FCE5D699BD39076869B841  : public RuntimeObject
{
public:
	// System.Int32 MoPubBinding/<>c__DisplayClass16_0::amount
	int32_t ___amount_0;

public:
	inline static int32_t get_offset_of_amount_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t2FAEADCC188F29AD24FCE5D699BD39076869B841, ___amount_0)); }
	inline int32_t get_amount_0() const { return ___amount_0; }
	inline int32_t* get_address_of_amount_0() { return &___amount_0; }
	inline void set_amount_0(int32_t value)
	{
		___amount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T2FAEADCC188F29AD24FCE5D699BD39076869B841_H
#ifndef JSON_T7BB22F20D633D4D916C1C0304C9A10F95E03ACEE_H
#define JSON_T7BB22F20D633D4D916C1C0304C9A10F95E03ACEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubInternal.ThirdParty.MiniJSON.Json
struct  Json_t7BB22F20D633D4D916C1C0304C9A10F95E03ACEE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T7BB22F20D633D4D916C1C0304C9A10F95E03ACEE_H
#ifndef PARSER_T0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A_H
#define PARSER_T0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubInternal.ThirdParty.MiniJSON.Json/Parser
struct  Parser_t0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A  : public RuntimeObject
{
public:
	// System.IO.StringReader MoPubInternal.ThirdParty.MiniJSON.Json/Parser::json
	StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_t0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A, ___json_1)); }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * get_json_1() const { return ___json_1; }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A_H
#ifndef SERIALIZER_TE675F7CC94ECD45F1704AD2C5405782E92FB4A03_H
#define SERIALIZER_TE675F7CC94ECD45F1704AD2C5405782E92FB4A03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubInternal.ThirdParty.MiniJSON.Json/Serializer
struct  Serializer_tE675F7CC94ECD45F1704AD2C5405782E92FB4A03  : public RuntimeObject
{
public:
	// System.Text.StringBuilder MoPubInternal.ThirdParty.MiniJSON.Json/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_tE675F7CC94ECD45F1704AD2C5405782E92FB4A03, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_TE675F7CC94ECD45F1704AD2C5405782E92FB4A03_H
#ifndef U3CU3EC_T884EB5636ABD137BEC61C62EA0C338FE7E263CEE_H
#define U3CU3EC_T884EB5636ABD137BEC61C62EA0C338FE7E263CEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubManager/<>c
struct  U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE_StaticFields
{
public:
	// MoPubManager/<>c MoPubManager/<>c::<>9
	U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.String> MoPubManager/<>c::<>9__81_0
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___U3CU3E9__81_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__81_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE_StaticFields, ___U3CU3E9__81_0_1)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_U3CU3E9__81_0_1() const { return ___U3CU3E9__81_0_1; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_U3CU3E9__81_0_1() { return &___U3CU3E9__81_0_1; }
	inline void set_U3CU3E9__81_0_1(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___U3CU3E9__81_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__81_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T884EB5636ABD137BEC61C62EA0C338FE7E263CEE_H
#ifndef PARTNERAPI_TE951FC71130AC792F11C902F0116AF6EF8226E6C_H
#define PARTNERAPI_TE951FC71130AC792F11C902F0116AF6EF8226E6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubiOS/PartnerApi
struct  PartnerApi_tE951FC71130AC792F11C902F0116AF6EF8226E6C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTNERAPI_TE951FC71130AC792F11C902F0116AF6EF8226E6C_H
#ifndef DICTIONARY_2_T9140A71329927AE4FD0F3CF4D4D66668EBE151EA_H
#define DICTIONARY_2_T9140A71329927AE4FD0F3CF4D4D66668EBE151EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___entries_1)); }
	inline EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tED6676A6535375B72FAC3BE58CAB9DC30BE201CD* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___keys_7)); }
	inline KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t9BB02C59F6A73E760F875CCD8AA6D1A1ACE61970 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ___values_8)); }
	inline ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD * get_values_8() const { return ___values_8; }
	inline ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t059E2E9F339047F683CA7038812E624189E7F0CD * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T9140A71329927AE4FD0F3CF4D4D66668EBE151EA_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VERSION_T59FF964E54F90842CCAC62E4E5829813C2ABC2AE_H
#define VERSION_T59FF964E54F90842CCAC62E4E5829813C2ABC2AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Version
struct  Version_t59FF964E54F90842CCAC62E4E5829813C2ABC2AE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T59FF964E54F90842CCAC62E4E5829813C2ABC2AE_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T5344ACDF15042B279793901AFBA9E7175E147661_H
#define U3CU3EC__DISPLAYCLASS4_0_T5344ACDF15042B279793901AFBA9E7175E147661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Version/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t5344ACDF15042B279793901AFBA9E7175E147661  : public RuntimeObject
{
public:
	// System.Int32 Version/<>c__DisplayClass4_0::piece
	int32_t ___piece_0;

public:
	inline static int32_t get_offset_of_piece_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5344ACDF15042B279793901AFBA9E7175E147661, ___piece_0)); }
	inline int32_t get_piece_0() const { return ___piece_0; }
	inline int32_t* get_address_of_piece_0() { return &___piece_0; }
	inline void set_piece_0(int32_t value)
	{
		___piece_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T5344ACDF15042B279793901AFBA9E7175E147661_H
#ifndef MOPUBANDROID_T19EF36F6558BF081F1E458C391C93C24EC69247C_H
#define MOPUBANDROID_T19EF36F6558BF081F1E458C391C93C24EC69247C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubAndroid
struct  MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C  : public MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434
{
public:

public:
};

struct MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass MoPubAndroid::PluginClass
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___PluginClass_4;
	// System.Collections.Generic.Dictionary`2<System.String,MoPubAndroidBanner> MoPubAndroid::BannerPluginsDict
	Dictionary_2_tAA807BD4DFDBC31BB8F9CF538DE207457F6EEFF6 * ___BannerPluginsDict_5;
	// System.Collections.Generic.Dictionary`2<System.String,MoPubAndroidInterstitial> MoPubAndroid::InterstitialPluginsDict
	Dictionary_2_tF1D3EF35F4BEFABA4F50501ADAFDB926C03C07B9 * ___InterstitialPluginsDict_6;
	// System.Collections.Generic.Dictionary`2<System.String,MoPubAndroidRewardedVideo> MoPubAndroid::RewardedVideoPluginsDict
	Dictionary_2_t13EFE8B9F65DAA94704EC73D62FD719DFE42A3D6 * ___RewardedVideoPluginsDict_7;

public:
	inline static int32_t get_offset_of_PluginClass_4() { return static_cast<int32_t>(offsetof(MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields, ___PluginClass_4)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_PluginClass_4() const { return ___PluginClass_4; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_PluginClass_4() { return &___PluginClass_4; }
	inline void set_PluginClass_4(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___PluginClass_4 = value;
		Il2CppCodeGenWriteBarrier((&___PluginClass_4), value);
	}

	inline static int32_t get_offset_of_BannerPluginsDict_5() { return static_cast<int32_t>(offsetof(MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields, ___BannerPluginsDict_5)); }
	inline Dictionary_2_tAA807BD4DFDBC31BB8F9CF538DE207457F6EEFF6 * get_BannerPluginsDict_5() const { return ___BannerPluginsDict_5; }
	inline Dictionary_2_tAA807BD4DFDBC31BB8F9CF538DE207457F6EEFF6 ** get_address_of_BannerPluginsDict_5() { return &___BannerPluginsDict_5; }
	inline void set_BannerPluginsDict_5(Dictionary_2_tAA807BD4DFDBC31BB8F9CF538DE207457F6EEFF6 * value)
	{
		___BannerPluginsDict_5 = value;
		Il2CppCodeGenWriteBarrier((&___BannerPluginsDict_5), value);
	}

	inline static int32_t get_offset_of_InterstitialPluginsDict_6() { return static_cast<int32_t>(offsetof(MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields, ___InterstitialPluginsDict_6)); }
	inline Dictionary_2_tF1D3EF35F4BEFABA4F50501ADAFDB926C03C07B9 * get_InterstitialPluginsDict_6() const { return ___InterstitialPluginsDict_6; }
	inline Dictionary_2_tF1D3EF35F4BEFABA4F50501ADAFDB926C03C07B9 ** get_address_of_InterstitialPluginsDict_6() { return &___InterstitialPluginsDict_6; }
	inline void set_InterstitialPluginsDict_6(Dictionary_2_tF1D3EF35F4BEFABA4F50501ADAFDB926C03C07B9 * value)
	{
		___InterstitialPluginsDict_6 = value;
		Il2CppCodeGenWriteBarrier((&___InterstitialPluginsDict_6), value);
	}

	inline static int32_t get_offset_of_RewardedVideoPluginsDict_7() { return static_cast<int32_t>(offsetof(MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields, ___RewardedVideoPluginsDict_7)); }
	inline Dictionary_2_t13EFE8B9F65DAA94704EC73D62FD719DFE42A3D6 * get_RewardedVideoPluginsDict_7() const { return ___RewardedVideoPluginsDict_7; }
	inline Dictionary_2_t13EFE8B9F65DAA94704EC73D62FD719DFE42A3D6 ** get_address_of_RewardedVideoPluginsDict_7() { return &___RewardedVideoPluginsDict_7; }
	inline void set_RewardedVideoPluginsDict_7(Dictionary_2_t13EFE8B9F65DAA94704EC73D62FD719DFE42A3D6 * value)
	{
		___RewardedVideoPluginsDict_7 = value;
		Il2CppCodeGenWriteBarrier((&___RewardedVideoPluginsDict_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBANDROID_T19EF36F6558BF081F1E458C391C93C24EC69247C_H
#ifndef ADVANCEDBIDDER_T3209FC9467A82285415AD05166EFF2D878249B5F_H
#define ADVANCEDBIDDER_T3209FC9467A82285415AD05166EFF2D878249B5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/AdvancedBidder
struct  AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F  : public ThirdPartyNetwork_t0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6
{
public:

public:
};

struct AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields
{
public:
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::AdColony
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___AdColony_2;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::AdMob
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___AdMob_3;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::AppLovin
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___AppLovin_4;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::Facebook
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___Facebook_5;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::OnebyAOL
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___OnebyAOL_6;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::Tapjoy
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___Tapjoy_7;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::Unity
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___Unity_8;
	// MoPubBase/AdvancedBidder MoPubBase/AdvancedBidder::Vungle
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * ___Vungle_9;

public:
	inline static int32_t get_offset_of_AdColony_2() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___AdColony_2)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_AdColony_2() const { return ___AdColony_2; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_AdColony_2() { return &___AdColony_2; }
	inline void set_AdColony_2(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___AdColony_2 = value;
		Il2CppCodeGenWriteBarrier((&___AdColony_2), value);
	}

	inline static int32_t get_offset_of_AdMob_3() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___AdMob_3)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_AdMob_3() const { return ___AdMob_3; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_AdMob_3() { return &___AdMob_3; }
	inline void set_AdMob_3(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___AdMob_3 = value;
		Il2CppCodeGenWriteBarrier((&___AdMob_3), value);
	}

	inline static int32_t get_offset_of_AppLovin_4() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___AppLovin_4)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_AppLovin_4() const { return ___AppLovin_4; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_AppLovin_4() { return &___AppLovin_4; }
	inline void set_AppLovin_4(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___AppLovin_4 = value;
		Il2CppCodeGenWriteBarrier((&___AppLovin_4), value);
	}

	inline static int32_t get_offset_of_Facebook_5() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___Facebook_5)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_Facebook_5() const { return ___Facebook_5; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_Facebook_5() { return &___Facebook_5; }
	inline void set_Facebook_5(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___Facebook_5 = value;
		Il2CppCodeGenWriteBarrier((&___Facebook_5), value);
	}

	inline static int32_t get_offset_of_OnebyAOL_6() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___OnebyAOL_6)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_OnebyAOL_6() const { return ___OnebyAOL_6; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_OnebyAOL_6() { return &___OnebyAOL_6; }
	inline void set_OnebyAOL_6(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___OnebyAOL_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnebyAOL_6), value);
	}

	inline static int32_t get_offset_of_Tapjoy_7() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___Tapjoy_7)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_Tapjoy_7() const { return ___Tapjoy_7; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_Tapjoy_7() { return &___Tapjoy_7; }
	inline void set_Tapjoy_7(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___Tapjoy_7 = value;
		Il2CppCodeGenWriteBarrier((&___Tapjoy_7), value);
	}

	inline static int32_t get_offset_of_Unity_8() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___Unity_8)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_Unity_8() const { return ___Unity_8; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_Unity_8() { return &___Unity_8; }
	inline void set_Unity_8(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___Unity_8 = value;
		Il2CppCodeGenWriteBarrier((&___Unity_8), value);
	}

	inline static int32_t get_offset_of_Vungle_9() { return static_cast<int32_t>(offsetof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields, ___Vungle_9)); }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * get_Vungle_9() const { return ___Vungle_9; }
	inline AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F ** get_address_of_Vungle_9() { return &___Vungle_9; }
	inline void set_Vungle_9(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F * value)
	{
		___Vungle_9 = value;
		Il2CppCodeGenWriteBarrier((&___Vungle_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDBIDDER_T3209FC9467A82285415AD05166EFF2D878249B5F_H
#ifndef MEDIATIONSETTING_TEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292_H
#define MEDIATIONSETTING_TEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/MediationSetting
struct  MediationSetting_tEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292  : public Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATIONSETTING_TEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292_H
#ifndef REWARD_TA184FAE40098AA9E1FD8F34C17E1B7259B62E940_H
#define REWARD_TA184FAE40098AA9E1FD8F34C17E1B7259B62E940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/Reward
struct  Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940 
{
public:
	// System.String MoPubBase/Reward::Label
	String_t* ___Label_0;
	// System.Int32 MoPubBase/Reward::Amount
	int32_t ___Amount_1;

public:
	inline static int32_t get_offset_of_Label_0() { return static_cast<int32_t>(offsetof(Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940, ___Label_0)); }
	inline String_t* get_Label_0() const { return ___Label_0; }
	inline String_t** get_address_of_Label_0() { return &___Label_0; }
	inline void set_Label_0(String_t* value)
	{
		___Label_0 = value;
		Il2CppCodeGenWriteBarrier((&___Label_0), value);
	}

	inline static int32_t get_offset_of_Amount_1() { return static_cast<int32_t>(offsetof(Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940, ___Amount_1)); }
	inline int32_t get_Amount_1() const { return ___Amount_1; }
	inline int32_t* get_address_of_Amount_1() { return &___Amount_1; }
	inline void set_Amount_1(int32_t value)
	{
		___Amount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MoPubBase/Reward
struct Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940_marshaled_pinvoke
{
	char* ___Label_0;
	int32_t ___Amount_1;
};
// Native definition for COM marshalling of MoPubBase/Reward
struct Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940_marshaled_com
{
	Il2CppChar* ___Label_0;
	int32_t ___Amount_1;
};
#endif // REWARD_TA184FAE40098AA9E1FD8F34C17E1B7259B62E940_H
#ifndef REWARDEDNETWORK_TC6E36B7D6221CF727C508BC2344D8ED60FE46A39_H
#define REWARDEDNETWORK_TC6E36B7D6221CF727C508BC2344D8ED60FE46A39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/RewardedNetwork
struct  RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39  : public ThirdPartyNetwork_t0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6
{
public:

public:
};

struct RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields
{
public:
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::AdColony
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___AdColony_2;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::AdMob
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___AdMob_3;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::AppLovin
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___AppLovin_4;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::Chartboost
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___Chartboost_5;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::Facebook
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___Facebook_6;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::IronSource
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___IronSource_7;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::OnebyAOL
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___OnebyAOL_8;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::Tapjoy
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___Tapjoy_9;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::Unity
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___Unity_10;
	// MoPubBase/RewardedNetwork MoPubBase/RewardedNetwork::Vungle
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * ___Vungle_11;

public:
	inline static int32_t get_offset_of_AdColony_2() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___AdColony_2)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_AdColony_2() const { return ___AdColony_2; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_AdColony_2() { return &___AdColony_2; }
	inline void set_AdColony_2(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___AdColony_2 = value;
		Il2CppCodeGenWriteBarrier((&___AdColony_2), value);
	}

	inline static int32_t get_offset_of_AdMob_3() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___AdMob_3)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_AdMob_3() const { return ___AdMob_3; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_AdMob_3() { return &___AdMob_3; }
	inline void set_AdMob_3(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___AdMob_3 = value;
		Il2CppCodeGenWriteBarrier((&___AdMob_3), value);
	}

	inline static int32_t get_offset_of_AppLovin_4() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___AppLovin_4)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_AppLovin_4() const { return ___AppLovin_4; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_AppLovin_4() { return &___AppLovin_4; }
	inline void set_AppLovin_4(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___AppLovin_4 = value;
		Il2CppCodeGenWriteBarrier((&___AppLovin_4), value);
	}

	inline static int32_t get_offset_of_Chartboost_5() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___Chartboost_5)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_Chartboost_5() const { return ___Chartboost_5; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_Chartboost_5() { return &___Chartboost_5; }
	inline void set_Chartboost_5(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___Chartboost_5 = value;
		Il2CppCodeGenWriteBarrier((&___Chartboost_5), value);
	}

	inline static int32_t get_offset_of_Facebook_6() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___Facebook_6)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_Facebook_6() const { return ___Facebook_6; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_Facebook_6() { return &___Facebook_6; }
	inline void set_Facebook_6(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___Facebook_6 = value;
		Il2CppCodeGenWriteBarrier((&___Facebook_6), value);
	}

	inline static int32_t get_offset_of_IronSource_7() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___IronSource_7)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_IronSource_7() const { return ___IronSource_7; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_IronSource_7() { return &___IronSource_7; }
	inline void set_IronSource_7(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___IronSource_7 = value;
		Il2CppCodeGenWriteBarrier((&___IronSource_7), value);
	}

	inline static int32_t get_offset_of_OnebyAOL_8() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___OnebyAOL_8)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_OnebyAOL_8() const { return ___OnebyAOL_8; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_OnebyAOL_8() { return &___OnebyAOL_8; }
	inline void set_OnebyAOL_8(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___OnebyAOL_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnebyAOL_8), value);
	}

	inline static int32_t get_offset_of_Tapjoy_9() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___Tapjoy_9)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_Tapjoy_9() const { return ___Tapjoy_9; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_Tapjoy_9() { return &___Tapjoy_9; }
	inline void set_Tapjoy_9(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___Tapjoy_9 = value;
		Il2CppCodeGenWriteBarrier((&___Tapjoy_9), value);
	}

	inline static int32_t get_offset_of_Unity_10() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___Unity_10)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_Unity_10() const { return ___Unity_10; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_Unity_10() { return &___Unity_10; }
	inline void set_Unity_10(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___Unity_10 = value;
		Il2CppCodeGenWriteBarrier((&___Unity_10), value);
	}

	inline static int32_t get_offset_of_Vungle_11() { return static_cast<int32_t>(offsetof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields, ___Vungle_11)); }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * get_Vungle_11() const { return ___Vungle_11; }
	inline RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 ** get_address_of_Vungle_11() { return &___Vungle_11; }
	inline void set_Vungle_11(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39 * value)
	{
		___Vungle_11 = value;
		Il2CppCodeGenWriteBarrier((&___Vungle_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDEDNETWORK_TC6E36B7D6221CF727C508BC2344D8ED60FE46A39_H
#ifndef SDKCONFIGURATION_T614929921CB6AF3E437C5F45E29415131E0767CC_H
#define SDKCONFIGURATION_T614929921CB6AF3E437C5F45E29415131E0767CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/SdkConfiguration
struct  SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC 
{
public:
	// System.String MoPubBase/SdkConfiguration::AdUnitId
	String_t* ___AdUnitId_0;
	// MoPubBase/AdvancedBidder[] MoPubBase/SdkConfiguration::AdvancedBidders
	AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08* ___AdvancedBidders_1;
	// MoPubBase/MediationSetting[] MoPubBase/SdkConfiguration::MediationSettings
	MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B* ___MediationSettings_2;
	// MoPubBase/RewardedNetwork[] MoPubBase/SdkConfiguration::NetworksToInit
	RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828* ___NetworksToInit_3;

public:
	inline static int32_t get_offset_of_AdUnitId_0() { return static_cast<int32_t>(offsetof(SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC, ___AdUnitId_0)); }
	inline String_t* get_AdUnitId_0() const { return ___AdUnitId_0; }
	inline String_t** get_address_of_AdUnitId_0() { return &___AdUnitId_0; }
	inline void set_AdUnitId_0(String_t* value)
	{
		___AdUnitId_0 = value;
		Il2CppCodeGenWriteBarrier((&___AdUnitId_0), value);
	}

	inline static int32_t get_offset_of_AdvancedBidders_1() { return static_cast<int32_t>(offsetof(SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC, ___AdvancedBidders_1)); }
	inline AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08* get_AdvancedBidders_1() const { return ___AdvancedBidders_1; }
	inline AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08** get_address_of_AdvancedBidders_1() { return &___AdvancedBidders_1; }
	inline void set_AdvancedBidders_1(AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08* value)
	{
		___AdvancedBidders_1 = value;
		Il2CppCodeGenWriteBarrier((&___AdvancedBidders_1), value);
	}

	inline static int32_t get_offset_of_MediationSettings_2() { return static_cast<int32_t>(offsetof(SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC, ___MediationSettings_2)); }
	inline MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B* get_MediationSettings_2() const { return ___MediationSettings_2; }
	inline MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B** get_address_of_MediationSettings_2() { return &___MediationSettings_2; }
	inline void set_MediationSettings_2(MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B* value)
	{
		___MediationSettings_2 = value;
		Il2CppCodeGenWriteBarrier((&___MediationSettings_2), value);
	}

	inline static int32_t get_offset_of_NetworksToInit_3() { return static_cast<int32_t>(offsetof(SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC, ___NetworksToInit_3)); }
	inline RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828* get_NetworksToInit_3() const { return ___NetworksToInit_3; }
	inline RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828** get_address_of_NetworksToInit_3() { return &___NetworksToInit_3; }
	inline void set_NetworksToInit_3(RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828* value)
	{
		___NetworksToInit_3 = value;
		Il2CppCodeGenWriteBarrier((&___NetworksToInit_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of MoPubBase/SdkConfiguration
struct SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC_marshaled_pinvoke
{
	char* ___AdUnitId_0;
	AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08* ___AdvancedBidders_1;
	MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B* ___MediationSettings_2;
	RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828* ___NetworksToInit_3;
};
// Native definition for COM marshalling of MoPubBase/SdkConfiguration
struct SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC_marshaled_com
{
	Il2CppChar* ___AdUnitId_0;
	AdvancedBidderU5BU5D_t567E35BBC91F95B1D495C3E64D824FD17D4D1D08* ___AdvancedBidders_1;
	MediationSettingU5BU5D_t9DA792F5B2D3B8253C09932E24D2FBDA0669B64B* ___MediationSettings_2;
	RewardedNetworkU5BU5D_tE4F01C90E257C3E3E8E4B9D179991D4E7102D828* ___NetworksToInit_3;
};
#endif // SDKCONFIGURATION_T614929921CB6AF3E437C5F45E29415131E0767CC_H
#ifndef MOPUBIOS_T5336352AEDC51AE1C211780684459196A821F8EF_H
#define MOPUBIOS_T5336352AEDC51AE1C211780684459196A821F8EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubiOS
struct  MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF  : public MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434
{
public:

public:
};

struct MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,MoPubBinding> MoPubiOS::PluginsDict
	Dictionary_2_t318F315F43DFED97F57079D2C81F0C6844934105 * ___PluginsDict_4;

public:
	inline static int32_t get_offset_of_PluginsDict_4() { return static_cast<int32_t>(offsetof(MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF_StaticFields, ___PluginsDict_4)); }
	inline Dictionary_2_t318F315F43DFED97F57079D2C81F0C6844934105 * get_PluginsDict_4() const { return ___PluginsDict_4; }
	inline Dictionary_2_t318F315F43DFED97F57079D2C81F0C6844934105 ** get_address_of_PluginsDict_4() { return &___PluginsDict_4; }
	inline void set_PluginsDict_4(Dictionary_2_t318F315F43DFED97F57079D2C81F0C6844934105 * value)
	{
		___PluginsDict_4 = value;
		Il2CppCodeGenWriteBarrier((&___PluginsDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBIOS_T5336352AEDC51AE1C211780684459196A821F8EF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MOPUB_T24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_H
#define MOPUB_T24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPub
struct  MoPub_t24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B  : public MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF
{
public:

public:
};

struct MoPub_t24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_StaticFields
{
public:
	// System.String MoPub::_sdkName
	String_t* ____sdkName_5;

public:
	inline static int32_t get_offset_of__sdkName_5() { return static_cast<int32_t>(offsetof(MoPub_t24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_StaticFields, ____sdkName_5)); }
	inline String_t* get__sdkName_5() const { return ____sdkName_5; }
	inline String_t** get_address_of__sdkName_5() { return &____sdkName_5; }
	inline void set__sdkName_5(String_t* value)
	{
		____sdkName_5 = value;
		Il2CppCodeGenWriteBarrier((&____sdkName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUB_T24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_H
#ifndef LOCATIONAWARENESS_T68DED34FBE08B9C2F51BC5C32260BFFE3866F82A_H
#define LOCATIONAWARENESS_T68DED34FBE08B9C2F51BC5C32260BFFE3866F82A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubAndroid/LocationAwareness
struct  LocationAwareness_t68DED34FBE08B9C2F51BC5C32260BFFE3866F82A 
{
public:
	// System.Int32 MoPubAndroid/LocationAwareness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LocationAwareness_t68DED34FBE08B9C2F51BC5C32260BFFE3866F82A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATIONAWARENESS_T68DED34FBE08B9C2F51BC5C32260BFFE3866F82A_H
#ifndef ADPOSITION_T898F2DD245A700B8347236C2FE5C0D985BEA1CBC_H
#define ADPOSITION_T898F2DD245A700B8347236C2FE5C0D985BEA1CBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/AdPosition
struct  AdPosition_t898F2DD245A700B8347236C2FE5C0D985BEA1CBC 
{
public:
	// System.Int32 MoPubBase/AdPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdPosition_t898F2DD245A700B8347236C2FE5C0D985BEA1CBC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADPOSITION_T898F2DD245A700B8347236C2FE5C0D985BEA1CBC_H
#ifndef BANNERTYPE_TAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D_H
#define BANNERTYPE_TAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/BannerType
struct  BannerType_tAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D 
{
public:
	// System.Int32 MoPubBase/BannerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BannerType_tAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERTYPE_TAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D_H
#ifndef STATUS_TAED3DD1226569F5163EC949B3122954568D72088_H
#define STATUS_TAED3DD1226569F5163EC949B3122954568D72088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/Consent/Status
struct  Status_tAED3DD1226569F5163EC949B3122954568D72088 
{
public:
	// System.Int32 MoPubBase/Consent/Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_tAED3DD1226569F5163EC949B3122954568D72088, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_TAED3DD1226569F5163EC949B3122954568D72088_H
#ifndef LOGLEVEL_T87C01FC67D8140064648E4DA8CB2E386C8B48C78_H
#define LOGLEVEL_T87C01FC67D8140064648E4DA8CB2E386C8B48C78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/LogLevel
struct  LogLevel_t87C01FC67D8140064648E4DA8CB2E386C8B48C78 
{
public:
	// System.Int32 MoPubBase/LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t87C01FC67D8140064648E4DA8CB2E386C8B48C78, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T87C01FC67D8140064648E4DA8CB2E386C8B48C78_H
#ifndef ADCOLONY_T3E15E19AA634C0490C3254D901EBACDAA09DA354_H
#define ADCOLONY_T3E15E19AA634C0490C3254D901EBACDAA09DA354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/MediationSetting/AdColony
struct  AdColony_t3E15E19AA634C0490C3254D901EBACDAA09DA354  : public MediationSetting_tEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADCOLONY_T3E15E19AA634C0490C3254D901EBACDAA09DA354_H
#ifndef ADMOB_TE2283B9AE381513DDF31A41D8448DFFF7D5179FD_H
#define ADMOB_TE2283B9AE381513DDF31A41D8448DFFF7D5179FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/MediationSetting/AdMob
struct  AdMob_tE2283B9AE381513DDF31A41D8448DFFF7D5179FD  : public MediationSetting_tEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADMOB_TE2283B9AE381513DDF31A41D8448DFFF7D5179FD_H
#ifndef CHARTBOOST_T9CFEBBA38D90B48AD6251E188A1F3858BB159BE5_H
#define CHARTBOOST_T9CFEBBA38D90B48AD6251E188A1F3858BB159BE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/MediationSetting/Chartboost
struct  Chartboost_t9CFEBBA38D90B48AD6251E188A1F3858BB159BE5  : public MediationSetting_tEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARTBOOST_T9CFEBBA38D90B48AD6251E188A1F3858BB159BE5_H
#ifndef VUNGLE_TEFC63E945C693E2F31C326E8EFA3363256DA366B_H
#define VUNGLE_TEFC63E945C693E2F31C326E8EFA3363256DA366B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBase/MediationSetting/Vungle
struct  Vungle_tEFC63E945C693E2F31C326E8EFA3363256DA366B  : public MediationSetting_tEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUNGLE_TEFC63E945C693E2F31C326E8EFA3363256DA366B_H
#ifndef MOPUBBINDING_TD988EBF8147CD79862A2AB965DBF1BD50838D86B_H
#define MOPUBBINDING_TD988EBF8147CD79862A2AB965DBF1BD50838D86B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubBinding
struct  MoPubBinding_tD988EBF8147CD79862A2AB965DBF1BD50838D86B  : public RuntimeObject
{
public:
	// MoPubBase/Reward MoPubBinding::SelectedReward
	Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940  ___SelectedReward_0;
	// System.String MoPubBinding::_adUnitId
	String_t* ____adUnitId_1;

public:
	inline static int32_t get_offset_of_SelectedReward_0() { return static_cast<int32_t>(offsetof(MoPubBinding_tD988EBF8147CD79862A2AB965DBF1BD50838D86B, ___SelectedReward_0)); }
	inline Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940  get_SelectedReward_0() const { return ___SelectedReward_0; }
	inline Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940 * get_address_of_SelectedReward_0() { return &___SelectedReward_0; }
	inline void set_SelectedReward_0(Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940  value)
	{
		___SelectedReward_0 = value;
	}

	inline static int32_t get_offset_of__adUnitId_1() { return static_cast<int32_t>(offsetof(MoPubBinding_tD988EBF8147CD79862A2AB965DBF1BD50838D86B, ____adUnitId_1)); }
	inline String_t* get__adUnitId_1() const { return ____adUnitId_1; }
	inline String_t** get_address_of__adUnitId_1() { return &____adUnitId_1; }
	inline void set__adUnitId_1(String_t* value)
	{
		____adUnitId_1 = value;
		Il2CppCodeGenWriteBarrier((&____adUnitId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBBINDING_TD988EBF8147CD79862A2AB965DBF1BD50838D86B_H
#ifndef TOKEN_T95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F_H
#define TOKEN_T95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubInternal.ThirdParty.MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F 
{
public:
	// System.Int32 MoPubInternal.ThirdParty.MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ABSTRACTNATIVEAD_T5DE929EC86F8AF299CCB450857B4EAC76E86125C_H
#define ABSTRACTNATIVEAD_T5DE929EC86F8AF299CCB450857B4EAC76E86125C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AbstractNativeAd
struct  AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String AbstractNativeAd::AdUnitId
	String_t* ___AdUnitId_4;
	// UnityEngine.UI.Text AbstractNativeAd::Title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Title_5;
	// UnityEngine.UI.Text AbstractNativeAd::Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Text_6;
	// UnityEngine.UI.Text AbstractNativeAd::CallToAction
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___CallToAction_7;
	// UnityEngine.Renderer AbstractNativeAd::MainImage
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___MainImage_8;
	// UnityEngine.Renderer AbstractNativeAd::IconImage
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___IconImage_9;
	// UnityEngine.Renderer AbstractNativeAd::PrivacyInformationIconImage
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___PrivacyInformationIconImage_10;

public:
	inline static int32_t get_offset_of_AdUnitId_4() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___AdUnitId_4)); }
	inline String_t* get_AdUnitId_4() const { return ___AdUnitId_4; }
	inline String_t** get_address_of_AdUnitId_4() { return &___AdUnitId_4; }
	inline void set_AdUnitId_4(String_t* value)
	{
		___AdUnitId_4 = value;
		Il2CppCodeGenWriteBarrier((&___AdUnitId_4), value);
	}

	inline static int32_t get_offset_of_Title_5() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___Title_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Title_5() const { return ___Title_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Title_5() { return &___Title_5; }
	inline void set_Title_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Title_5 = value;
		Il2CppCodeGenWriteBarrier((&___Title_5), value);
	}

	inline static int32_t get_offset_of_Text_6() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___Text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Text_6() const { return ___Text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Text_6() { return &___Text_6; }
	inline void set_Text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Text_6 = value;
		Il2CppCodeGenWriteBarrier((&___Text_6), value);
	}

	inline static int32_t get_offset_of_CallToAction_7() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___CallToAction_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_CallToAction_7() const { return ___CallToAction_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_CallToAction_7() { return &___CallToAction_7; }
	inline void set_CallToAction_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___CallToAction_7 = value;
		Il2CppCodeGenWriteBarrier((&___CallToAction_7), value);
	}

	inline static int32_t get_offset_of_MainImage_8() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___MainImage_8)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_MainImage_8() const { return ___MainImage_8; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_MainImage_8() { return &___MainImage_8; }
	inline void set_MainImage_8(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___MainImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___MainImage_8), value);
	}

	inline static int32_t get_offset_of_IconImage_9() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___IconImage_9)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_IconImage_9() const { return ___IconImage_9; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_IconImage_9() { return &___IconImage_9; }
	inline void set_IconImage_9(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___IconImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___IconImage_9), value);
	}

	inline static int32_t get_offset_of_PrivacyInformationIconImage_10() { return static_cast<int32_t>(offsetof(AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C, ___PrivacyInformationIconImage_10)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_PrivacyInformationIconImage_10() const { return ___PrivacyInformationIconImage_10; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_PrivacyInformationIconImage_10() { return &___PrivacyInformationIconImage_10; }
	inline void set_PrivacyInformationIconImage_10(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___PrivacyInformationIconImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___PrivacyInformationIconImage_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTNATIVEAD_T5DE929EC86F8AF299CCB450857B4EAC76E86125C_H
#ifndef MOPUBMANAGER_TF71FADE1E71150753744E54568484653491B89B2_H
#define MOPUBMANAGER_TF71FADE1E71150753744E54568484653491B89B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubManager
struct  MoPubManager_tF71FADE1E71150753744E54568484653491B89B2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields
{
public:
	// MoPubManager MoPubManager::<Instance>k__BackingField
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2 * ___U3CInstanceU3Ek__BackingField_4;
	// System.Action`1<System.String> MoPubManager::OnSdkInitializedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnSdkInitializedEvent_5;
	// System.Action`2<System.String,System.Single> MoPubManager::OnAdLoadedEvent
	Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * ___OnAdLoadedEvent_6;
	// System.Action`2<System.String,System.String> MoPubManager::OnAdFailedEvent
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___OnAdFailedEvent_7;
	// System.Action`1<System.String> MoPubManager::OnAdClickedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnAdClickedEvent_8;
	// System.Action`1<System.String> MoPubManager::OnAdExpandedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnAdExpandedEvent_9;
	// System.Action`1<System.String> MoPubManager::OnAdCollapsedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnAdCollapsedEvent_10;
	// System.Action`1<System.String> MoPubManager::OnInterstitialLoadedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnInterstitialLoadedEvent_11;
	// System.Action`2<System.String,System.String> MoPubManager::OnInterstitialFailedEvent
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___OnInterstitialFailedEvent_12;
	// System.Action`1<System.String> MoPubManager::OnInterstitialDismissedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnInterstitialDismissedEvent_13;
	// System.Action`1<System.String> MoPubManager::OnInterstitialExpiredEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnInterstitialExpiredEvent_14;
	// System.Action`1<System.String> MoPubManager::OnInterstitialShownEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnInterstitialShownEvent_15;
	// System.Action`1<System.String> MoPubManager::OnInterstitialClickedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnInterstitialClickedEvent_16;
	// System.Action`1<System.String> MoPubManager::OnRewardedVideoLoadedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnRewardedVideoLoadedEvent_17;
	// System.Action`2<System.String,System.String> MoPubManager::OnRewardedVideoFailedEvent
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___OnRewardedVideoFailedEvent_18;
	// System.Action`1<System.String> MoPubManager::OnRewardedVideoExpiredEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnRewardedVideoExpiredEvent_19;
	// System.Action`1<System.String> MoPubManager::OnRewardedVideoShownEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnRewardedVideoShownEvent_20;
	// System.Action`1<System.String> MoPubManager::OnRewardedVideoClickedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnRewardedVideoClickedEvent_21;
	// System.Action`2<System.String,System.String> MoPubManager::OnRewardedVideoFailedToPlayEvent
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___OnRewardedVideoFailedToPlayEvent_22;
	// System.Action`3<System.String,System.String,System.Single> MoPubManager::OnRewardedVideoReceivedRewardEvent
	Action_3_tE127DFBA1D4D2336BD8421F6F97C63D472CEFE6C * ___OnRewardedVideoReceivedRewardEvent_23;
	// System.Action`1<System.String> MoPubManager::OnRewardedVideoClosedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnRewardedVideoClosedEvent_24;
	// System.Action`1<System.String> MoPubManager::OnRewardedVideoLeavingApplicationEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnRewardedVideoLeavingApplicationEvent_25;
	// System.Action`3<MoPubBase/Consent/Status,MoPubBase/Consent/Status,System.Boolean> MoPubManager::OnConsentStatusChangedEvent
	Action_3_t94FD4401FD739E608B7CC5F8918FAF026C04DA76 * ___OnConsentStatusChangedEvent_26;
	// System.Action MoPubManager::OnConsentDialogLoadedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnConsentDialogLoadedEvent_27;
	// System.Action`1<System.String> MoPubManager::OnConsentDialogFailedEvent
	Action_1_t19CAF500829927B30EC94F39939F749E4919669E * ___OnConsentDialogFailedEvent_28;
	// System.Action MoPubManager::OnConsentDialogShownEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnConsentDialogShownEvent_29;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline MoPubManager_tF71FADE1E71150753744E54568484653491B89B2 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline MoPubManager_tF71FADE1E71150753744E54568484653491B89B2 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_OnSdkInitializedEvent_5() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnSdkInitializedEvent_5)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnSdkInitializedEvent_5() const { return ___OnSdkInitializedEvent_5; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnSdkInitializedEvent_5() { return &___OnSdkInitializedEvent_5; }
	inline void set_OnSdkInitializedEvent_5(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnSdkInitializedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnSdkInitializedEvent_5), value);
	}

	inline static int32_t get_offset_of_OnAdLoadedEvent_6() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnAdLoadedEvent_6)); }
	inline Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * get_OnAdLoadedEvent_6() const { return ___OnAdLoadedEvent_6; }
	inline Action_2_tD053D0BAA29F99F68652212454226A67BE949793 ** get_address_of_OnAdLoadedEvent_6() { return &___OnAdLoadedEvent_6; }
	inline void set_OnAdLoadedEvent_6(Action_2_tD053D0BAA29F99F68652212454226A67BE949793 * value)
	{
		___OnAdLoadedEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoadedEvent_6), value);
	}

	inline static int32_t get_offset_of_OnAdFailedEvent_7() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnAdFailedEvent_7)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_OnAdFailedEvent_7() const { return ___OnAdFailedEvent_7; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_OnAdFailedEvent_7() { return &___OnAdFailedEvent_7; }
	inline void set_OnAdFailedEvent_7(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___OnAdFailedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedEvent_7), value);
	}

	inline static int32_t get_offset_of_OnAdClickedEvent_8() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnAdClickedEvent_8)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnAdClickedEvent_8() const { return ___OnAdClickedEvent_8; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnAdClickedEvent_8() { return &___OnAdClickedEvent_8; }
	inline void set_OnAdClickedEvent_8(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnAdClickedEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClickedEvent_8), value);
	}

	inline static int32_t get_offset_of_OnAdExpandedEvent_9() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnAdExpandedEvent_9)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnAdExpandedEvent_9() const { return ___OnAdExpandedEvent_9; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnAdExpandedEvent_9() { return &___OnAdExpandedEvent_9; }
	inline void set_OnAdExpandedEvent_9(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnAdExpandedEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdExpandedEvent_9), value);
	}

	inline static int32_t get_offset_of_OnAdCollapsedEvent_10() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnAdCollapsedEvent_10)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnAdCollapsedEvent_10() const { return ___OnAdCollapsedEvent_10; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnAdCollapsedEvent_10() { return &___OnAdCollapsedEvent_10; }
	inline void set_OnAdCollapsedEvent_10(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnAdCollapsedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdCollapsedEvent_10), value);
	}

	inline static int32_t get_offset_of_OnInterstitialLoadedEvent_11() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnInterstitialLoadedEvent_11)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnInterstitialLoadedEvent_11() const { return ___OnInterstitialLoadedEvent_11; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnInterstitialLoadedEvent_11() { return &___OnInterstitialLoadedEvent_11; }
	inline void set_OnInterstitialLoadedEvent_11(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnInterstitialLoadedEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterstitialLoadedEvent_11), value);
	}

	inline static int32_t get_offset_of_OnInterstitialFailedEvent_12() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnInterstitialFailedEvent_12)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_OnInterstitialFailedEvent_12() const { return ___OnInterstitialFailedEvent_12; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_OnInterstitialFailedEvent_12() { return &___OnInterstitialFailedEvent_12; }
	inline void set_OnInterstitialFailedEvent_12(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___OnInterstitialFailedEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterstitialFailedEvent_12), value);
	}

	inline static int32_t get_offset_of_OnInterstitialDismissedEvent_13() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnInterstitialDismissedEvent_13)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnInterstitialDismissedEvent_13() const { return ___OnInterstitialDismissedEvent_13; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnInterstitialDismissedEvent_13() { return &___OnInterstitialDismissedEvent_13; }
	inline void set_OnInterstitialDismissedEvent_13(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnInterstitialDismissedEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterstitialDismissedEvent_13), value);
	}

	inline static int32_t get_offset_of_OnInterstitialExpiredEvent_14() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnInterstitialExpiredEvent_14)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnInterstitialExpiredEvent_14() const { return ___OnInterstitialExpiredEvent_14; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnInterstitialExpiredEvent_14() { return &___OnInterstitialExpiredEvent_14; }
	inline void set_OnInterstitialExpiredEvent_14(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnInterstitialExpiredEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterstitialExpiredEvent_14), value);
	}

	inline static int32_t get_offset_of_OnInterstitialShownEvent_15() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnInterstitialShownEvent_15)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnInterstitialShownEvent_15() const { return ___OnInterstitialShownEvent_15; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnInterstitialShownEvent_15() { return &___OnInterstitialShownEvent_15; }
	inline void set_OnInterstitialShownEvent_15(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnInterstitialShownEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterstitialShownEvent_15), value);
	}

	inline static int32_t get_offset_of_OnInterstitialClickedEvent_16() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnInterstitialClickedEvent_16)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnInterstitialClickedEvent_16() const { return ___OnInterstitialClickedEvent_16; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnInterstitialClickedEvent_16() { return &___OnInterstitialClickedEvent_16; }
	inline void set_OnInterstitialClickedEvent_16(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnInterstitialClickedEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnInterstitialClickedEvent_16), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoLoadedEvent_17() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoLoadedEvent_17)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnRewardedVideoLoadedEvent_17() const { return ___OnRewardedVideoLoadedEvent_17; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnRewardedVideoLoadedEvent_17() { return &___OnRewardedVideoLoadedEvent_17; }
	inline void set_OnRewardedVideoLoadedEvent_17(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnRewardedVideoLoadedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoLoadedEvent_17), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoFailedEvent_18() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoFailedEvent_18)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_OnRewardedVideoFailedEvent_18() const { return ___OnRewardedVideoFailedEvent_18; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_OnRewardedVideoFailedEvent_18() { return &___OnRewardedVideoFailedEvent_18; }
	inline void set_OnRewardedVideoFailedEvent_18(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___OnRewardedVideoFailedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoFailedEvent_18), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoExpiredEvent_19() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoExpiredEvent_19)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnRewardedVideoExpiredEvent_19() const { return ___OnRewardedVideoExpiredEvent_19; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnRewardedVideoExpiredEvent_19() { return &___OnRewardedVideoExpiredEvent_19; }
	inline void set_OnRewardedVideoExpiredEvent_19(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnRewardedVideoExpiredEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoExpiredEvent_19), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoShownEvent_20() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoShownEvent_20)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnRewardedVideoShownEvent_20() const { return ___OnRewardedVideoShownEvent_20; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnRewardedVideoShownEvent_20() { return &___OnRewardedVideoShownEvent_20; }
	inline void set_OnRewardedVideoShownEvent_20(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnRewardedVideoShownEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoShownEvent_20), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoClickedEvent_21() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoClickedEvent_21)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnRewardedVideoClickedEvent_21() const { return ___OnRewardedVideoClickedEvent_21; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnRewardedVideoClickedEvent_21() { return &___OnRewardedVideoClickedEvent_21; }
	inline void set_OnRewardedVideoClickedEvent_21(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnRewardedVideoClickedEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoClickedEvent_21), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoFailedToPlayEvent_22() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoFailedToPlayEvent_22)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_OnRewardedVideoFailedToPlayEvent_22() const { return ___OnRewardedVideoFailedToPlayEvent_22; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_OnRewardedVideoFailedToPlayEvent_22() { return &___OnRewardedVideoFailedToPlayEvent_22; }
	inline void set_OnRewardedVideoFailedToPlayEvent_22(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___OnRewardedVideoFailedToPlayEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoFailedToPlayEvent_22), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoReceivedRewardEvent_23() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoReceivedRewardEvent_23)); }
	inline Action_3_tE127DFBA1D4D2336BD8421F6F97C63D472CEFE6C * get_OnRewardedVideoReceivedRewardEvent_23() const { return ___OnRewardedVideoReceivedRewardEvent_23; }
	inline Action_3_tE127DFBA1D4D2336BD8421F6F97C63D472CEFE6C ** get_address_of_OnRewardedVideoReceivedRewardEvent_23() { return &___OnRewardedVideoReceivedRewardEvent_23; }
	inline void set_OnRewardedVideoReceivedRewardEvent_23(Action_3_tE127DFBA1D4D2336BD8421F6F97C63D472CEFE6C * value)
	{
		___OnRewardedVideoReceivedRewardEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoReceivedRewardEvent_23), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoClosedEvent_24() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoClosedEvent_24)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnRewardedVideoClosedEvent_24() const { return ___OnRewardedVideoClosedEvent_24; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnRewardedVideoClosedEvent_24() { return &___OnRewardedVideoClosedEvent_24; }
	inline void set_OnRewardedVideoClosedEvent_24(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnRewardedVideoClosedEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoClosedEvent_24), value);
	}

	inline static int32_t get_offset_of_OnRewardedVideoLeavingApplicationEvent_25() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnRewardedVideoLeavingApplicationEvent_25)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnRewardedVideoLeavingApplicationEvent_25() const { return ___OnRewardedVideoLeavingApplicationEvent_25; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnRewardedVideoLeavingApplicationEvent_25() { return &___OnRewardedVideoLeavingApplicationEvent_25; }
	inline void set_OnRewardedVideoLeavingApplicationEvent_25(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnRewardedVideoLeavingApplicationEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___OnRewardedVideoLeavingApplicationEvent_25), value);
	}

	inline static int32_t get_offset_of_OnConsentStatusChangedEvent_26() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnConsentStatusChangedEvent_26)); }
	inline Action_3_t94FD4401FD739E608B7CC5F8918FAF026C04DA76 * get_OnConsentStatusChangedEvent_26() const { return ___OnConsentStatusChangedEvent_26; }
	inline Action_3_t94FD4401FD739E608B7CC5F8918FAF026C04DA76 ** get_address_of_OnConsentStatusChangedEvent_26() { return &___OnConsentStatusChangedEvent_26; }
	inline void set_OnConsentStatusChangedEvent_26(Action_3_t94FD4401FD739E608B7CC5F8918FAF026C04DA76 * value)
	{
		___OnConsentStatusChangedEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnConsentStatusChangedEvent_26), value);
	}

	inline static int32_t get_offset_of_OnConsentDialogLoadedEvent_27() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnConsentDialogLoadedEvent_27)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnConsentDialogLoadedEvent_27() const { return ___OnConsentDialogLoadedEvent_27; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnConsentDialogLoadedEvent_27() { return &___OnConsentDialogLoadedEvent_27; }
	inline void set_OnConsentDialogLoadedEvent_27(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnConsentDialogLoadedEvent_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnConsentDialogLoadedEvent_27), value);
	}

	inline static int32_t get_offset_of_OnConsentDialogFailedEvent_28() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnConsentDialogFailedEvent_28)); }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E * get_OnConsentDialogFailedEvent_28() const { return ___OnConsentDialogFailedEvent_28; }
	inline Action_1_t19CAF500829927B30EC94F39939F749E4919669E ** get_address_of_OnConsentDialogFailedEvent_28() { return &___OnConsentDialogFailedEvent_28; }
	inline void set_OnConsentDialogFailedEvent_28(Action_1_t19CAF500829927B30EC94F39939F749E4919669E * value)
	{
		___OnConsentDialogFailedEvent_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnConsentDialogFailedEvent_28), value);
	}

	inline static int32_t get_offset_of_OnConsentDialogShownEvent_29() { return static_cast<int32_t>(offsetof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields, ___OnConsentDialogShownEvent_29)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnConsentDialogShownEvent_29() const { return ___OnConsentDialogShownEvent_29; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnConsentDialogShownEvent_29() { return &___OnConsentDialogShownEvent_29; }
	inline void set_OnConsentDialogShownEvent_29(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnConsentDialogShownEvent_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnConsentDialogShownEvent_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBMANAGER_TF71FADE1E71150753744E54568484653491B89B2_H
#ifndef MOPUBNATIVEADSTOGGLER_T1309AA0C1DEFD2A7856E2826BFB84BE7DDED0D6B_H
#define MOPUBNATIVEADSTOGGLER_T1309AA0C1DEFD2A7856E2826BFB84BE7DDED0D6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubNativeAdsToggler
struct  MoPubNativeAdsToggler_t1309AA0C1DEFD2A7856E2826BFB84BE7DDED0D6B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBNATIVEADSTOGGLER_T1309AA0C1DEFD2A7856E2826BFB84BE7DDED0D6B_H
#ifndef NATIVEADCLICKHANDLER_T84ED61614F1D40E0C84F221EC848863788B552B1_H
#define NATIVEADCLICKHANDLER_T84ED61614F1D40E0C84F221EC848863788B552B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeAdClickHandler
struct  NativeAdClickHandler_t84ED61614F1D40E0C84F221EC848863788B552B1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEADCLICKHANDLER_T84ED61614F1D40E0C84F221EC848863788B552B1_H
#ifndef TEST_TBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246_H
#define TEST_TBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// test
struct  test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] test::adIds
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___adIds_4;
	// System.Boolean test::isSdkInitialized
	bool ___isSdkInitialized_5;
	// System.Boolean test::isRewardedVideoLoaded
	bool ___isRewardedVideoLoaded_6;
	// UnityEngine.UI.Text test::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_7;

public:
	inline static int32_t get_offset_of_adIds_4() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___adIds_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_adIds_4() const { return ___adIds_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_adIds_4() { return &___adIds_4; }
	inline void set_adIds_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___adIds_4 = value;
		Il2CppCodeGenWriteBarrier((&___adIds_4), value);
	}

	inline static int32_t get_offset_of_isSdkInitialized_5() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___isSdkInitialized_5)); }
	inline bool get_isSdkInitialized_5() const { return ___isSdkInitialized_5; }
	inline bool* get_address_of_isSdkInitialized_5() { return &___isSdkInitialized_5; }
	inline void set_isSdkInitialized_5(bool value)
	{
		___isSdkInitialized_5 = value;
	}

	inline static int32_t get_offset_of_isRewardedVideoLoaded_6() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___isRewardedVideoLoaded_6)); }
	inline bool get_isRewardedVideoLoaded_6() const { return ___isRewardedVideoLoaded_6; }
	inline bool* get_address_of_isRewardedVideoLoaded_6() { return &___isRewardedVideoLoaded_6; }
	inline void set_isRewardedVideoLoaded_6(bool value)
	{
		___isRewardedVideoLoaded_6 = value;
	}

	inline static int32_t get_offset_of_text_7() { return static_cast<int32_t>(offsetof(test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246, ___text_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_7() const { return ___text_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_7() { return &___text_7; }
	inline void set_text_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_7 = value;
		Il2CppCodeGenWriteBarrier((&___text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEST_TBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246_H
#ifndef MOPUBSTATICNATIVEAD_TEBAE86ACAA4F50E7AED6DFA0B5453AD88728F8FF_H
#define MOPUBSTATICNATIVEAD_TEBAE86ACAA4F50E7AED6DFA0B5453AD88728F8FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoPubStaticNativeAd
struct  MoPubStaticNativeAd_tEBAE86ACAA4F50E7AED6DFA0B5453AD88728F8FF  : public AbstractNativeAd_t5DE929EC86F8AF299CCB450857B4EAC76E86125C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOPUBSTATICNATIVEAD_TEBAE86ACAA4F50E7AED6DFA0B5453AD88728F8FF_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C), -1, sizeof(MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2500[4] = 
{
	MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields::get_offset_of_PluginClass_4(),
	MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields::get_offset_of_BannerPluginsDict_5(),
	MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields::get_offset_of_InterstitialPluginsDict_6(),
	MoPubAndroid_t19EF36F6558BF081F1E458C391C93C24EC69247C_StaticFields::get_offset_of_RewardedVideoPluginsDict_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (LocationAwareness_t68DED34FBE08B9C2F51BC5C32260BFFE3866F82A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2501[4] = 
{
	LocationAwareness_t68DED34FBE08B9C2F51BC5C32260BFFE3866F82A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (PartnerApi_t7E4194C1D7C65B28ECC9ABFCD9562E952A709C35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (MoPubAndroidBanner_t654343028A79D05456C69A16B1A1CA00880413C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	MoPubAndroidBanner_t654343028A79D05456C69A16B1A1CA00880413C1::get_offset_of__bannerPlugin_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (MoPubAndroidInterstitial_t087506AE648C1295D36DCA16338F0DA08F4E5702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	MoPubAndroidInterstitial_t087506AE648C1295D36DCA16338F0DA08F4E5702::get_offset_of__interstitialPlugin_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E), -1, sizeof(MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E_StaticFields::get_offset_of_PluginClass_0(),
	MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E::get_offset_of__plugin_1(),
	MoPubAndroidRewardedVideo_t6CAD3CA835B573B2F6BB715FACB436EE440DB08E::get_offset_of__rewardsDict_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434), -1, sizeof(MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2506[4] = 
{
	MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields::get_offset_of_U3CConsentLanguageCodeU3Ek__BackingField_0(),
	0,
	MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields::get_offset_of_moPubSDKVersion_2(),
	MoPubBase_t4D3B07D4B8711A296C5D4659B8CA2CD1E7A13434_StaticFields::get_offset_of__pluginName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (AdPosition_t898F2DD245A700B8347236C2FE5C0D985BEA1CBC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[8] = 
{
	AdPosition_t898F2DD245A700B8347236C2FE5C0D985BEA1CBC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (Consent_t48C9151900FAD71E084A92493B822A1514CA681E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Status_tAED3DD1226569F5163EC949B3122954568D72088)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[6] = 
{
	Status_tAED3DD1226569F5163EC949B3122954568D72088::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (Strings_t2C56F329FE6C514D38DB9364EE4F5DA76BC895E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (BannerType_tAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2511[5] = 
{
	BannerType_tAF12AE8766C81E8B10D3B46AEE1DDEFE5FC7CF9D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (LogLevel_t87C01FC67D8140064648E4DA8CB2E386C8B48C78)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2512[9] = 
{
	LogLevel_t87C01FC67D8140064648E4DA8CB2E386C8B48C78::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[4] = 
{
	SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC::get_offset_of_AdUnitId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC::get_offset_of_AdvancedBidders_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC::get_offset_of_MediationSettings_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SdkConfiguration_t614929921CB6AF3E437C5F45E29415131E0767CC::get_offset_of_NetworksToInit_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23), -1, sizeof(U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2514[3] = 
{
	U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
	U3CU3Ec_tEE373A695EB3F985F0545801FD3DE5468A905E23_StaticFields::get_offset_of_U3CU3E9__9_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (MediationSetting_tEBF37DAB7FCDD0B04FF2EBE3219619811BCAA292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (AdColony_t3E15E19AA634C0490C3254D901EBACDAA09DA354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (AdMob_tE2283B9AE381513DDF31A41D8448DFFF7D5179FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (Chartboost_t9CFEBBA38D90B48AD6251E188A1F3858BB159BE5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (Vungle_tEFC63E945C693E2F31C326E8EFA3363256DA366B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940)+ sizeof (RuntimeObject), sizeof(Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2520[2] = 
{
	Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940::get_offset_of_Label_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Reward_tA184FAE40098AA9E1FD8F34C17E1B7259B62E940::get_offset_of_Amount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ThirdPartyNetwork_t0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[1] = 
{
	ThirdPartyNetwork_t0DC573787D498EFB32B53EC84D3C82EBB7DE4FE6::get_offset_of__name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F), -1, sizeof(AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2522[9] = 
{
	0,
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_AdColony_2(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_AdMob_3(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_AppLovin_4(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_Facebook_5(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_OnebyAOL_6(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_Tapjoy_7(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_Unity_8(),
	AdvancedBidder_t3209FC9467A82285415AD05166EFF2D878249B5F_StaticFields::get_offset_of_Vungle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39), -1, sizeof(RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2523[11] = 
{
	0,
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_AdColony_2(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_AdMob_3(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_AppLovin_4(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_Chartboost_5(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_Facebook_6(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_IronSource_7(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_OnebyAOL_8(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_Tapjoy_9(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_Unity_10(),
	RewardedNetwork_tC6E36B7D6221CF727C508BC2344D8ED60FE46A39_StaticFields::get_offset_of_Vungle_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (MoPubBinding_tD988EBF8147CD79862A2AB965DBF1BD50838D86B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	MoPubBinding_tD988EBF8147CD79862A2AB965DBF1BD50838D86B::get_offset_of_SelectedReward_0(),
	MoPubBinding_tD988EBF8147CD79862A2AB965DBF1BD50838D86B::get_offset_of__adUnitId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (U3CU3Ec__DisplayClass16_0_t2FAEADCC188F29AD24FCE5D699BD39076869B841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[1] = 
{
	U3CU3Ec__DisplayClass16_0_t2FAEADCC188F29AD24FCE5D699BD39076869B841::get_offset_of_amount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4), -1, sizeof(U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[3] = 
{
	U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields::get_offset_of_U3CU3E9__16_0_1(),
	U3CU3Ec_t097B9A03AB814D5DF37EA6E41CB77CB28FFAB0E4_StaticFields::get_offset_of_U3CU3E9__16_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF), -1, sizeof(MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[1] = 
{
	MoPubiOS_t5336352AEDC51AE1C211780684459196A821F8EF_StaticFields::get_offset_of_PluginsDict_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (PartnerApi_tE951FC71130AC792F11C902F0116AF6EF8226E6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (MoPubNativeAdsToggler_t1309AA0C1DEFD2A7856E2826BFB84BE7DDED0D6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (MoPubStaticNativeAd_tEBAE86ACAA4F50E7AED6DFA0B5453AD88728F8FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (NativeAdClickHandler_t84ED61614F1D40E0C84F221EC848863788B552B1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (Version_t59FF964E54F90842CCAC62E4E5829813C2ABC2AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (U3CU3Ec__DisplayClass4_0_t5344ACDF15042B279793901AFBA9E7175E147661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[1] = 
{
	U3CU3Ec__DisplayClass4_0_t5344ACDF15042B279793901AFBA9E7175E147661::get_offset_of_piece_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (MoPub_t24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B), -1, sizeof(MoPub_t24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2534[1] = 
{
	MoPub_t24AD81F192F8A64D9C2CE7E1C3E21FF18C26066B_StaticFields::get_offset_of__sdkName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (MoPubManager_tF71FADE1E71150753744E54568484653491B89B2), -1, sizeof(MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2535[26] = 
{
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_4(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnSdkInitializedEvent_5(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnAdLoadedEvent_6(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnAdFailedEvent_7(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnAdClickedEvent_8(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnAdExpandedEvent_9(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnAdCollapsedEvent_10(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnInterstitialLoadedEvent_11(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnInterstitialFailedEvent_12(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnInterstitialDismissedEvent_13(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnInterstitialExpiredEvent_14(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnInterstitialShownEvent_15(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnInterstitialClickedEvent_16(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoLoadedEvent_17(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoFailedEvent_18(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoExpiredEvent_19(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoShownEvent_20(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoClickedEvent_21(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoFailedToPlayEvent_22(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoReceivedRewardEvent_23(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoClosedEvent_24(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnRewardedVideoLeavingApplicationEvent_25(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnConsentStatusChangedEvent_26(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnConsentDialogLoadedEvent_27(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnConsentDialogFailedEvent_28(),
	MoPubManager_tF71FADE1E71150753744E54568484653491B89B2_StaticFields::get_offset_of_OnConsentDialogShownEvent_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE), -1, sizeof(U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2536[2] = 
{
	U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t884EB5636ABD137BEC61C62EA0C338FE7E263CEE_StaticFields::get_offset_of_U3CU3E9__81_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[4] = 
{
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_adIds_4(),
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_isSdkInitialized_5(),
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_isRewardedVideoLoaded_6(),
	test_tBCA4AC3C1D45BA0DD176A21EC1F45D70CE6F1246::get_offset_of_text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (Json_t7BB22F20D633D4D916C1C0304C9A10F95E03ACEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (Parser_t0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[2] = 
{
	0,
	Parser_t0ACB561E0F3B60D4575B97D295CDAE4D5A0BBE9A::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (TOKEN_t95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[13] = 
{
	TOKEN_t95BC10161C1F25E821EB8D66B8EBCFFB0A59F07F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (Serializer_tE675F7CC94ECD45F1704AD2C5405782E92FB4A03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[1] = 
{
	Serializer_tE675F7CC94ECD45F1704AD2C5405782E92FB4A03::get_offset_of_builder_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
