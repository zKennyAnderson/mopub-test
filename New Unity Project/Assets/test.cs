﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour
{
    //Rewarded video ad id
    private string[] adIds = {"a7ce9433d65a47cb92bec410d4acdefd"};

    private bool isSdkInitialized = false;
    private bool isRewardedVideoLoaded = false;

    [SerializeField]
    private Text text;

    private void Start()
    {
        var anyAdUnitId = adIds[0];
        MoPub.InitializeSdk(new MoPubBase.SdkConfiguration {
            AdUnitId = anyAdUnitId,
        });

        MoPubManager.OnSdkInitializedEvent += OnSdkInitializedEvent;

        //Rewarded Video Events
        MoPubManager.OnRewardedVideoClickedEvent += OnRewardedVideoClickedEvent;
        MoPubManager.OnRewardedVideoClosedEvent += OnRewardedVideoClosedEvent;
        MoPubManager.OnRewardedVideoExpiredEvent += OnRewardedVideoExpiredEvent;
        MoPubManager.OnRewardedVideoFailedEvent += OnRewardedVideoFailedEvent;
        MoPubManager.OnRewardedVideoFailedToPlayEvent += OnRewardedVideoFailedToPlayEvent;
        MoPubManager.OnRewardedVideoLeavingApplicationEvent += OnRewardedVideoLeavingApplicationEvent;
        MoPubManager.OnRewardedVideoLoadedEvent += OnRewardedVideoLoadedEvent;
        MoPubManager.OnRewardedVideoReceivedRewardEvent += OnRewardedVideoReceivedRewardEvent;
        MoPubManager.OnRewardedVideoShownEvent += OnRewardedVideoShownEvent;

        text.text = "";
    }

    public void OnSdkInitializedEvent(string s){
        isSdkInitialized = true;

        MoPub.LoadRewardedVideoPluginsForAdUnits(adIds);
        PrepareRewardedVideo();

        text.text += "\nmopub: SDK initialized";
    }

    public void PrepareRewardedVideo(){
        if (!isSdkInitialized){
            
            return;
        }
        
        text.text += "\nmopub: PrepareRewardedVideo";
        MoPub.RequestRewardedVideo(adIds[0]);
    }
    public bool CanShowRewardedVideo(){
        return MoPub.HasRewardedVideo(adIds[0]);
    }

    public void ShowRewardedVideo(){
        text.text += "\nmopub: ShowRewardedVideo";
        MoPub.ShowRewardedVideo(adIds[0]);
    }

    #region Rewarded Video callbacks

    public void OnRewardedVideoClickedEvent(string s){
        text.text += "\nmopub: Rewarded video clicked callback";
    }

    public void OnRewardedVideoClosedEvent(string s){
        text.text += "\nmopub: Rewarded video closed callback";
    }

    public void OnRewardedVideoExpiredEvent(string s){
        text.text += "\nmopub: Rewarded video expired callback";
    }

    public void OnRewardedVideoFailedEvent(string s, string e){
        text.text += "\nmopub: Rewarded video failed callback: " + e;
    }

    public void OnRewardedVideoFailedToPlayEvent(string s, string e){
        text.text += "\nmopub: Rewarded video failed to play callback: " + e;
    }

    public void OnRewardedVideoLeavingApplicationEvent(string s){
        text.text += "\nmopub: Rewarded video leaving application callback";
    }
    
    public void OnRewardedVideoLoadedEvent(string s){
        text.text += "\nmopub: Rewarded video loaded callback";
        isRewardedVideoLoaded = true;
    }

    public void OnRewardedVideoReceivedRewardEvent(string s, string s2, float f){
        text.text += "\nmopub: Rewarded video received reward callback";
    }

    public void OnRewardedVideoShownEvent(string s){
        text.text += "\nmopub: Rewarded video shown callback";
    }

    #endregion
}
